/*
 * File:   scheduler-sim.buildSrc.main/convention-base.gradle.kts
 * Date:    30.03.21, 09:12
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.build.src

/* Reasoning for changing build dir
I am aware that changing the output directory is frowned upon and Gradles default works very well.
However, I prefer this. Yes, that's it. Preference. Deal with it.

Build output can now be found in rootDir/build/<project-path>
I.e. A hypothetical project with the path :some:nested:project will place its output in rootDir/some-nested-project
For reference: by default this project would compile to rootDir/some/nested/project/build/

This is in no way written to be generally usable though. I rarely find myself nesting project and I often use
dashes within project names. deviating from this style can make thing more confusing, rather than less, as is the
intention and it will likely lead to conflicts in very large and deeply nested projects.

My reasoning behind this style: I prefer generated and written files separated. While Gradle does this to some
degree already, It has always irked me that there are generated files scattered all throughout subdirectories.
Using this Style, generated files will only ever appear in rootDir/build/, rootDir/.gradle/,rootdir/gradle/
Note that buildSrc is a bit of an exception, due to being essentially an entirely separate project. It's possible to
have buildSrc compile to the same directory but as I see it, buildSrc output isn't part of the project, but rather
part of gradle.*/
buildDir = if (project == rootProject) {
    rootDir.resolve("build/${project.name}")
} else {
    project.group = rootProject.group
    rootDir.resolve("build/${project.path.replace(":", "-").removePrefix("-")}")
}
