/*
 * File:   scheduler-sim.buildSrc.main/convention-kotlin.gradle.kts
 * Date:    30.03.21, 09:13
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.build.src

import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.extensions.DetektExtension
import org.gradle.accessors.dm.LibrariesForLibs
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// This allows access to version catalog in precompiled script plugins
// See: https://github.com/gradle/gradle/issues/15383
val libs = the<LibrariesForLibs>()

plugins {
    id("lafreakshow.build.src.convention-base")
    id("org.jetbrains.kotlin.jvm")
    id("io.gitlab.arturbosch.detekt")
    id("org.jetbrains.dokka")
}

repositories {
    mavenCentral()

    // This is where TornadoFX 2.0.0 is hosted
    maven("https://oss.sonatype.org/content/repositories/snapshots").mavenContent {
        snapshotsOnly()
    }

    // As URL suggests, kotlinx.* libraries live there
    maven("https://kotlin.bintray.com/kotlinx")
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(libs.versions.jvmTarget.get()))
}

the<DetektExtension>().apply {
    autoCorrect = false
    config.from(rootDir.resolve("config/detekt/detekt.yml"))
}

dependencies {
    implementation(platform(libs.kotlin.bom))

    testImplementation(platform(libs.junit.bom))
    testImplementation(libs.bundles.kotest.jvm)

    detektPlugins(libs.detekt.plugins.formatting)
}

val moduleName: String by extra

tasks {
    withType<Detekt>().configureEach {
        // Target version of the generated JVM bytecode. It is used for type resolution.
        this.jvmTarget = libs.versions.detektJvmTarget.get()
    }

    withType(Test::class.java).configureEach {
        doFirst {
            // This block allows us to easily configure javas logging system for test runs via files in config/test/
            val projectLoggingProps = rootProject.rootDir.resolve("config/test/${project.name}/logging.properties")
            val globalLoggingProps = rootProject.rootDir.resolve("config/test/logging.properties")

            // See if a prop file specific to the current project exists, if now, use the global prop file.
            val propsToUse = (if (projectLoggingProps.exists()) projectLoggingProps else globalLoggingProps)
                .relativeTo(workingDir)

            logger.lifecycle("Using Logging props from ${propsToUse.path}")
            jvmArgs(
                "-Djava.util.logging.config.file=${propsToUse.path}"
            )

            // Note: I've since removed the javafx and modularity plugins, so the code here doesn't work anymore.
            // It'll likely be necessary to replace it eventually. Probably by simply passing the appropriate cli
            // options to the test vm.
            //
            // There are often issues during testing with Java Modules because some libraries don't properly support
            // them. These are options to help with that.
            // extensions.configure(TestModuleOptions::class) {
            // If all else fails, we can always tell the modularity plugin that test should be run on the classpath.
            // This effectively circumvents the module system entirely.
            // runOnClasspath = true

            // These are here to non-modular libraries can be accesses via reflection.
            // addReads[moduleName] = "strikt.core"
            // addReads["kotlin.stdlib"] = "kotlinx.coroutines.core.jvm"
            // }
        }

        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }
    }

    withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = libs.versions.jvmTarget.get()

        /* Explanation and Reasoning for redirecting kotlin classes to java output dir
         As stated in the log message below, there is quite a bit of weirdness with Java modules and kotlin.
         Specifically, the Java compiler needs to know the location of kotlin class when compiling module.java files
         By Default kotlin and java class files end up in different output directories. There are multiple ways to
         deal with this problem but the simplest is to simply redirect kotlin class files to the java output
         directory.

         Note: This assumes that the only java files to compile will be module.java files. conflicts can arise if
         there is also java code in the project. But since I don't ever plan on using anything except Kotlin here,
         this is fine.

         Still, in the long run it would be nice to find a better solution that isn't essentially a hacky workaround.
         While I haven't found any so far, it is definetely possible that there may be issues with this solution and
         other gradle plugins. */
        val newDestination = destinationDir.parentFile.resolveSibling("java/${destinationDir.name}")
        val relOld = destinationDir.relativeTo(rootDir).path
        val relNew = newDestination.relativeTo(rootDir).path
        logger.info(
            "Changing Kotlin class dir from $relOld to $relNew. This is necessary due to weirdness with " +
                "kotlin and JPMS and works only in pure kotlin jvm projects"
        )
        destinationDir = newDestination
    }

    named("compileJava", JavaCompile::class.java).configure {
        // Not needed since we set a toolchain version earlier
        // options.release.set(jvmTarget.toInt())
    }
}
