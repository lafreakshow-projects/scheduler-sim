/*
 * File:   scheduler-sim.buildSrc.main/convention-root.gradle.kts
 * Date:    30.03.21, 09:13
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.build.src

import org.ajoberstar.reckon.gradle.ReckonExtension
import org.jetbrains.dokka.gradle.DokkaMultiModuleTask

repositories {
    maven("https://dl.bintray.com/kotlin/dokka")

    // Dokka needs these to pull in some dependencies
    mavenCentral()
    maven("https://kotlin.bintray.com/kotlinx")
}

plugins {
    id("lafreakshow.build.src.convention-base")
    id("org.jetbrains.dokka")
}

tasks.named("dokkaHtmlMultiModule", DokkaMultiModuleTask::class).configure {
    outputDirectory.set(rootProject.buildDir.resolve("documentation"))
}

afterEvaluate {
    /* On Reckon and its quirks
     Yes, this will likely only ever run in the root project anyway, but if it ever happens to *not* be the root
     project. It *will* crash the build. And reckon in general has a tendency to produce weird unhelp errors.
     Not necessarily because reckon can't error handle, but more so due to the way it achieves it's task.

     Notably, accessing project.version after reckon was applied but before it was configured will result in
     inexplicable nullpointer and type mismatch errors. This is because reckon replaces the normal project.version
     property with a provider.

     Similarly, failing to configure either scopeFromProp or snapshotfromProp/stageFromPropr will result in
     seemingly unexplainable missing property errors. This one could likely be handled a bit better by reckon
     but to be fair here, these errors do mention "scope" and/or "stage" not being set.
     */
    if (project == rootProject) {
        apply(plugin = "org.ajoberstar.reckon")

        the<ReckonExtension>().apply {
            scopeFromProp()
            //stageFromProp("dev", 'milestone', 'final')

            /* Reasoning for using Snapshot versioning
             Snaptshots are generally not how to do things in semantic versioning but having the version be
             dynamic and automatic with reckon presents some interesting quirks. Notably, *every* build will result
             in a jar with a unique name, which will pile up quickly during lengthy debugging dessions, since the
             version used in it's name will include a timestamp.

             In the past I used to configure jar tasks to only keep the 10 most recent jars by version (thankfully,
             semantic versions do order chronologically very nicely.)
             But this had some issues in itself and in the end I decided that I would likely never need an older
             builds result. Initially I configured jar tasks to leave out the timestamp (which only appears in
             intermediate versions anyway) but ultimately, this is way more convenient.

             The major problem with Snapshots is that releasing a snapshot means the consumer cannot rely on the same
             snapshot version always being the same binary, since a new snapshot can be released with the same exact
             name. However, in paxis, this isn't often an issue. People are unlikely to use "hot" dev version and as
             soon a new minor version is released via reckon, that particular snapshot version is consistent.
             (because reckon *never* allows versions to decrease. A version must always be higher than the previous)
             Generally, dev versions should always be expected to be somwhat inconsistent so the tradeoff is neglibale
             in my opinion. Additionally, since there is no automatic deployment, snapshots will probably only exist
             on a devs machine and never be released to the public.

             Note: Snapshots present a much larger issue when the project in question is a library. This project is
             self contained so even if a snapshot was released, its potential binary incompatiblity is unlikely to
             cause problems */
            snapshotFromProp()
        }

        tasks {
            val reckonTagCreate by getting
            reckonTagCreate.dependsOn("check")
        }
    }
}
