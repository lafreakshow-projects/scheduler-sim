/*
 * File:   scheduler-sim.buildSrc.main/convention-tornado.gradle.kts
 * Date:    30.03.21, 09:13
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.build.src

import org.gradle.accessors.dm.LibrariesForLibs

val libs = the<LibrariesForLibs>()

val osName = System.getProperty("os.name").toLowerCase()
val openjfxPlatform: String? = when {
    osName.contains("mac") -> "mac"
    osName.contains("win") -> "win"
    osName.contains("linux") -> "linux"
    else -> null
}
logger.lifecycle("OpenJFX Platform detected as $openjfxPlatform from os $osName")

plugins {
    id("lafreakshow.build.src.convention-kotlin")
}

dependencies {
    fun javaFXModule(name: String) {
        implementation(
            group = "org.openjfx",
            version = libs.versions.jfx.get(),
            classifier = openjfxPlatform,
            name = "javafx-$name"
        )
    }

    javaFXModule("base")
    javaFXModule("graphics")
    javaFXModule("controls")
    javaFXModule("media")
    javaFXModule("swing")
    javaFXModule("web")
    javaFXModule("fxml")

    implementation(libs.tornadoFX)
}
