/*
 * File:   scheduler-sim.sim-shared/build.gradle.kts
 * Date:    30.03.21, 08:57
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    id("lafreakshow.build.src.convention-tornado")
    application
}

dependencies {
    implementation(libs.bundles.ikonli)

    implementation(projects.simShared)
    implementation(projects.simBackend)
    implementation(projects.simLang)
    implementation(projects.simRecord)
}

application {
    applicationDefaultJvmArgs = listOf(
        // Required for TornadoFX to work since it uses reflection a lot.
        "--add-opens", "javafx.graphics/javafx.scene=tornadofx"
    )
    mainModule.set("lafreakshow.simulation.scheduler.gui")
    mainClass.set("lafreakshow.simulation.scheduler.gui.SchedulerSimAppKt")
}
