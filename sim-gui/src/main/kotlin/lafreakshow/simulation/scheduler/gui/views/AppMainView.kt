/*
 * File:   scheduler-sim.sim-gui.main/AppMainView.kt
 * Date:    30.03.21, 10:28
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.views

import javafx.geometry.Pos
import tornadofx.*
import kotlin.reflect.KClass

class AppMainView : View("Scheduler Simulation") {
    override val root = borderpane() {
        top<MenuBarView>()
        center<MainMenuView>()

        top.borderpaneConstraints {
            this.alignment = Pos.TOP_CENTER
        }
    }

    init {
        find<AppMainViewController>().setOwnView(this)
    }

    internal fun setActiveView(uiComponent: KClass<out UIComponent>) {
        root.center(uiComponent)
        currentStage?.sizeToScene()
        currentWindow?.centerOnScreen()
    }
}

class AppMainViewController : Controller() {
    // Normally, I would have the controller be injected with it's view, but here this could easily lead to cyclic
    // access errors as many views will use this controller, so instead I have the view be injected with it's
    // controller and assign itself to ownView. Normally, no view should call any method on this controller before
    // AppMainView is fully instantiated (because it just doesn't make sense), and therefore ownView will always
    // have a value when it is accessed.
    private var ownView: AppMainView by singleAssign()

    internal fun setOwnView(view: AppMainView) { ownView = view }

    fun changeView(newView: KClass<out UIComponent>) {
        ownView.setActiveView(newView)
    }

    inline fun <reified T : UIComponent> changeView() = changeView(T::class)
}
