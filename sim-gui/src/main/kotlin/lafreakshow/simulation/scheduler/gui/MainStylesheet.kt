/*
 * File:   scheduler-sim.sim-gui.main/MainStylesheet.kt
 * Date:    01.04.21, 07:48
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui

import javafx.scene.paint.Paint
import tornadofx.*

class MainStylesheet : Stylesheet() {
    init {
        Stylesheet.star {
            fontFamily = "Roboto"
            fontSize = 1.1.em
        }

        paddedBox {
            padding = tornadofx.box(10.px)
        }

        spacedBox {
            spacing = 10.0.px
            vgap = 5.0.px
            hgap = 5.0.px
        }

        infoViewGrid {
            hgap = 2.em
            vgap = 10.px
        }

        select(ikonliFontIcon, ikonliStackedFontIcon) {
            unsafe(iconColor, PropertyHolder.Raw("ladder(-fx-color, black 30%, derive(-fx-color, -63%) 31%)"))
            // iconColor set baseColor.ladder(Stop(0.30, Color.WHITE), Stop(.31, baseColor.derive(-.63)))
        }
    }

    companion object {
        // style classes that can be used to style Ikonli FontIcons.
        val ikonliFontIcon by cssclass("ikonli-font-icon")
        val ikonliStackedFontIcon by cssclass("stacked-ikonli-font-icon")

        val paddedBox by cssclass()
        val spacedBox by cssclass()
        val infoViewGrid by cssclass()
    }
}

val PropertyHolder.iconColor by cssproperty<Paint>("-fx-icon-color")
