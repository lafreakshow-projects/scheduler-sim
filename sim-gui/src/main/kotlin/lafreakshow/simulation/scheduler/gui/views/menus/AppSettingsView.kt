/*
 * File:   scheduler-sim.sim-gui.main/AppSettingsView.kt
 * Date:    06.06.21, 12:42
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.views.menus

import lafreakshow.simulation.scheduler.gui.MainStylesheet
import tornadofx.*

class AppSettingsView : View("Scheduler Simulation Settings") {
    override val root = gridpane {
        addClass(MainStylesheet.spacedBox, MainStylesheet.paddedBox)

        row {
            label("test")
            label("Test")
        }
        row {
            label("test")
            label("Testsdlfh,sjdhf")
        }
        row {
            label("test")
            label("Test")
        }
        row {
            label("testlolosdfnhiosdffg")
            label("Test")
        }
    }
}
