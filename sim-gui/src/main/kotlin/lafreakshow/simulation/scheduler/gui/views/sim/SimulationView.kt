/*
 * File:   scheduler-sim.sim-gui.main/SimulationView.kt
 * Date:    31.03.21, 16:53
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.views.sim

import lafreakshow.simulation.scheduler.gui.MainStylesheet
import lafreakshow.simulation.scheduler.gui.autoSize
import lafreakshow.simulation.scheduler.gui.components.dataDisplayGrid
import lafreakshow.simulation.scheduler.gui.components.titledBorder
import tornadofx.*
import javafx.geometry.Orientation
import javafx.scene.control.TableView
import javafx.scene.layout.Priority

class SimulationView : View() {
    override val root = vbox {
        addClass(MainStylesheet.spacedBox)
        hbox {
            vgrow = Priority.SOMETIMES
            paddingHorizontal = 10.0
            paddingTop = 10.0
            addClass(MainStylesheet.spacedBox)
            titledBorder("Sim Status") {
                contentBuilder {
                    hbox {
                        hgrow = Priority.SOMETIMES
                        addClass(MainStylesheet.spacedBox)
                        dataDisplayGrid {
                            label("Simulation Running...")
                            separator()
                            display("Time", "234")
                            display("Active Jobs", "5")
                            display("Waiting Jobs", "5")
                            display("Processors IDLE", "5")
                        }
                        separator(Orientation.VERTICAL)
                        dataDisplayGrid {
                            display("Simulation Time", "39478")
                            separator()
                            display("Most Desired Res.", "Some Ressource ID")
                            display("Least Desired Res.", "Some Resource ID")
                            display("Most active Job", "some job Name")
                            display("Most blocked Job", "some job Name")
                        }
                    }
                }
            }

            titledBorder("Processors") {
                val dummyProcessors = listOf(
                    DummyProcessor(0, "IDLE", 234, 3, 3),
                    DummyProcessor(1, "IDLE", 234, 3, 3),
                    DummyProcessor(2, "IDLE", 234, 3, 3),
                    DummyProcessor(3, "IDLE", 234, 3, 3),
                    DummyProcessor(4, "IDLE", 234, 3, 3),
                ).asObservable()

                // isCollapsible = false
                hgrow = Priority.ALWAYS
                contentBuilder {
                    tableview(dummyProcessors) {
                        this.columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
                        readonlyColumn("ID", DummyProcessor::id).autoSize()
                        readonlyColumn("Status", DummyProcessor::currentAction).autoSize(false)
                        readonlyColumn("Idle time", DummyProcessor::timeSpentIdling).autoSize(false)
                        readonlyColumn("Ctx switches", DummyProcessor::contextSwitches).autoSize(false)
                        readonlyColumn("Idle since", DummyProcessor::idleSince).autoSize(false)
                    }
                }
            }
        }
        hbox {
            paddingHorizontal = 10.0
            vgrow = Priority.ALWAYS
            addClass(MainStylesheet.spacedBox)
            val dummyResources = listOf(
                DummyResource("lol", 0, 4, 2),
                DummyResource("quark", 1, 4, 4),
                DummyResource("foo", 2, 1, 0),
                DummyResource("bar", 3, 1, 1)
            ).asObservable()

            val dummyJobs = listOf(
                DummyJob("loljob", 0, listOf(0, 4), "WORKING", .8),
                DummyJob("lel job", 1, listOf(2), "WAITING", .0),
                DummyJob("Foo job", 2, listOf(1, 3), "WORKING", .1),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
                DummyJob("donejob", 3, listOf(), "FINISHED", 1.0),
            ).asObservable()

            titledBorder("Resources") {
                hgrow = Priority.ALWAYS
                maxWidth = Double.MAX_VALUE

                contentBuilder {
                    tableview(dummyResources) {
                        this.columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
                        readonlyColumn("ID", DummyResource::id).autoSize()
                        readonlyColumn("Name", DummyResource::name).autoSize(false)
                        readonlyColumn("Total", DummyResource::totalNum).autoSize()
                        readonlyColumn("Free", DummyResource::freeNum).autoSize()
                        readonlyColumn("Free %", DummyResource::percentFree).useProgressBar().autoSize()
                    }
                }
            }
            titledBorder("Jobs") {
                hgrow = Priority.ALWAYS
                maxWidth = Double.MAX_VALUE
                val dummyResources = listOf(
                    DummyResource("lol", 0, 4, 2),
                    DummyResource("quark", 1, 4, 4),
                    DummyResource("foo", 2, 1, 0),
                    DummyResource("bar", 3, 1, 1)
                ).asObservable()

                contentBuilder {
                    val table = tableview(dummyJobs) {
                        this.columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY

                        readonlyColumn("ID", DummyJob::id).autoSize()
                        readonlyColumn("Name", DummyJob::name).autoSize(false)
                        readonlyColumn("Resources", DummyJob::resourcesLocked).autoSize()
                        readonlyColumn("Status", DummyJob::status).autoSize(false)
                        readonlyColumn("Completion", DummyJob::approximateCompletion).useProgressBar().autoSize()
                    }
                }
            }
        }
        titledpane("Event Log", find<EventLogView>().root) {}
    }
}

data class DummyResource(
    val name: String,
    val id: Int,
    val totalNum: Int,
    val freeNum: Int,
    val percentFree: Double = freeNum.toDouble() / totalNum.toDouble()
)

data class DummyJob(
    val name: String,
    val id: Int,
    val resourcesLocked: List<Int>,
    val status: String,
    val approximateCompletion: Double
)

data class DummyProcessor(
    val id: Int,
    val currentAction: String,
    val timeSpentIdling: Int,
    val contextSwitches: Int,
    val idleSince: Int
)
