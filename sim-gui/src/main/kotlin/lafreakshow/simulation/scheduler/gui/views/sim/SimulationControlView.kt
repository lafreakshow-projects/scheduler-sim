/*
 * File:   scheduler-sim.sim-gui.main/SimulationControlView.kt
 * Date:    05.06.21, 14:01
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.views.sim

import lafreakshow.simulation.scheduler.gui.MainStylesheet
import lafreakshow.simulation.scheduler.gui.iconColor
import org.kordamp.ikonli.javafx.FontIcon
import org.kordamp.ikonli.material2.Material2AL
import org.kordamp.ikonli.material2.Material2MZ
import tornadofx.*
import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.paint.Color
import lafreakshow.simulation.scheduler.gui.views.menus.AppSettingsView

class SimulationControlView : View() {

    val isPlayingProp = SimpleBooleanProperty(false)
    var isPlaying by isPlayingProp

    val playIcon = FontIcon(Material2MZ.PLAY_ARROW)
    val pauseIcon = FontIcon(Material2MZ.PAUSE)

    // the titledBorder is wrapped in an HBox to prevent a parent from expanding the bordered area. Instead it will be
    // centered within the HBox
    override val root = hbox {
        stylesheet {
            simulationControl {
                unsafe("-fx-background-color", PropertyHolder.Raw("derive(-fx-background, -5%)"))
                borderStyle = multi(BorderStrokeStyle.SOLID)
                borderWidth = multi(box(0.px, 0.px, 1.px, 0.px))
            }

            MainStylesheet.ikonliFontIcon {
                unsafe("-fx-icon-size", 20.px)
            }

            s("#stopButton") {
                MainStylesheet.ikonliFontIcon {
                    iconColor set Color.INDIANRED
                }
            }

            s("#playButton") {
                MainStylesheet.ikonliFontIcon {
                    iconColor set Color.SEAGREEN
                }
            }
        }

        addClass(MainStylesheet.paddedBox, simulationControl)

        alignment = Pos.CENTER

        hbox() {
            addClass(MainStylesheet.spacedBox)
            alignment = Pos.CENTER

            button("", FontIcon(Material2MZ.STOP)) {
                id = "stopButton"
                tooltip("Stop Simulation")
            }
            separator(Orientation.VERTICAL)

            button("", FontIcon(Material2MZ.SKIP_PREVIOUS)) { tooltip("Step Back") }
            button("", FontIcon(Material2MZ.PLAY_ARROW)) {
                id = "playButton"

                graphicProperty().bind(
                    isPlayingProp.objectBinding(isPlayingProp) {
                        if (it == true) pauseIcon else playIcon
                    }
                )

                action { isPlaying = !isPlaying }
                tooltip("Play/Pause")
            }
            button("", FontIcon(Material2MZ.SKIP_NEXT)) { tooltip("Step Forward") }
            separator(Orientation.VERTICAL)
            button("", FontIcon(Material2AL.FAST_FORWARD)) { tooltip("Get it done already!") }
        }

        spacer()
        hbox {
            addClass(MainStylesheet.spacedBox)
            alignment = Pos.CENTER

            button("Edit Simulation Parameters...")

            hbox {
                label("Sim Speed: ")
                spinner(1, 1000) {
                    prefWidth = 150.0
                }
                tooltip("time per simulation cycle in ms")

                alignment = Pos.CENTER
            }
            separator(Orientation.VERTICAL)
            button("", FontIcon(Material2MZ.SETTINGS)) {
                tooltip("Open Settings Dialog")
                action {
                    find<AppSettingsView>().openModal()
                }
            }
        }
    }

    companion object {
        val simulationControl by cssclass()
    }
}
