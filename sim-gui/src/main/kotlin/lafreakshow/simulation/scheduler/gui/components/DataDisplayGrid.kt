/*
 * File:   scheduler-sim.sim-gui.main/DataDisplayGrid.kt
 * Date:    01.04.21, 07:53
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.components

import lafreakshow.simulation.scheduler.gui.label
import tornadofx.*
import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import javafx.geometry.Orientation
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.Separator
import javafx.scene.layout.GridPane

class DataDisplayGrid : GridPane() {

    init {
        addClass(DataDisplayGridStylesheet.dataDisplayGrid)
    }

    fun display(left: Node, right: Node) {
        left.addClass(DataDisplayGridStylesheet.dataDisplayGridLeftNode)
        right.addClass(DataDisplayGridStylesheet.dataDisplayGridRightNode)
        this.addRow(this.rowCount, left, right)
    }

    fun display(label: String, node: Node) = display(Label(label), node)
    fun display(label: String, text: String) = display(Label(label), Label(text))
    fun display(label: String, observableText: ObservableValue<String>) = display(label, observableText.label())

    fun display(node: Node) {
        this.addRow(this.rowCount, node)
        node.addClass(DataDisplayGridStylesheet.dataDisplayGridWideNode)
        node.gridpaneConstraints { columnSpan = 2 }
    }

    fun button(label: String = "", graphic: Node? = null, builder: Button.() -> Unit = {}): Button {
        val button = if (graphic != null) Button(label, graphic) else Button(label)
        button.apply(builder)
        display(button)
        return button
    }

    fun label(text: String, graphic: Node? = null, builder: Label.() -> Unit): Label {
        val label = if (graphic != null) Label(text, graphic) else Label(text)
        label.apply(builder)
        display(label)
        return label
    }

    fun separator(builder: Separator.() -> Unit = {}): Separator {
        val sep = Separator(Orientation.HORIZONTAL)
        sep.apply(builder)
        display(sep)
        return sep
    }

    override fun getUserAgentStylesheet(): String = DataDisplayGridStylesheet().base64URL.toExternalForm()
}

fun EventTarget.dataDisplayGrid(builder: DataDisplayGrid.() -> Unit) = opcr(this, DataDisplayGrid(), builder)

class DataDisplayGridStylesheet : Stylesheet() {
    init {
        dataDisplayGrid {
            hgap = 1.5.em
            vgap = 10.0.px
        }
    }

    companion object {
        val dataDisplayGrid by cssclass()
        val dataDisplayGridLeftNode by cssclass()
        val dataDisplayGridRightNode by cssclass()
        val dataDisplayGridWideNode by cssclass()
    }
}
