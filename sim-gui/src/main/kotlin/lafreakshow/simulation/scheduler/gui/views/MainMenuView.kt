/*
 * File:   scheduler-sim.sim-gui.main/MainMenuView.kt
 * Date:    05.06.21, 13:59
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.views

import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.layout.Priority
import lafreakshow.simulation.scheduler.gui.ApplicationController
import lafreakshow.simulation.scheduler.gui.MainStylesheet
import tornadofx.*
import tornadofx.Stylesheet.Companion.box
import tornadofx.Stylesheet.Companion.button

class MainMenuView: View() {
    private val mainViewController by inject<AppMainViewController>()
    private val appController by inject<ApplicationController>()

    override val root: Parent = vbox {
        prefHeight = 500.0
        prefWidth = 400.0

        addClass(mainMenu, MainStylesheet.paddedBox)
        spacer {
            minHeight = 10.0
            maxHeight = 50.0
        }
        button("New Simulation").setOnAction { mainViewController.changeView<SimulationContainerView>() }
        button("Load Record")
        button("Preset Editor")
        button("Settings")
        spacer(Priority.SOMETIMES)
        button("Exit").setOnAction { appController.quitApplication() }
        spacer {
            minHeight = 10.0
            maxHeight = 50.0
        }
    }

    init {
        root.addClass(MainStylesheet.paddedBox, MainStylesheet.spacedBox, mainMenu)
        root.stylesheet {
            mainMenu {
                alignment = Pos.TOP_CENTER
            }

            s(mainMenu.child(button)) {
                this.prefWidth = 300.px
            }
        }
    }

    companion object {
        val mainMenu by cssclass()
    }
}
