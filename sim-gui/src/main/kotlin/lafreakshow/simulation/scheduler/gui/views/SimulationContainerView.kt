/*
 * File:   scheduler-sim.sim-gui.main/SimulationContainerView.kt
 * Date:    05.06.21, 14:04
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.views

import javafx.scene.Parent
import lafreakshow.simulation.scheduler.gui.views.sim.SimulationControlView
import lafreakshow.simulation.scheduler.gui.views.sim.SimulationView
import lafreakshow.simulation.scheduler.gui.views.sim.StatusBarView
import tornadofx.View
import tornadofx.borderpane

class SimulationContainerView : View() {
    override val root: Parent = borderpane {
        prefWidth = 1200.0
        prefHeight = 900.0

        top<SimulationControlView>()
        center<SimulationView>()
        bottom<StatusBarView>()
    }
}
