/*
 * File:   scheduler-sim.sim-gui.main/TitledBorder.kt
 * Date:    02.04.21, 11:53
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.components

import tornadofx.*
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventTarget
import javafx.geometry.HPos
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.layout.*
import javafx.scene.paint.Color


class TitledBorder(title: String, content: Node?, align: HPos = HPos.LEFT) : StackPane() {
    val titleProperty = SimpleStringProperty()
    var title by titleProperty

    val contentProperty = objectProperty<Node?>()
    var content by contentProperty

    val titleHAlignProperty = objectProperty<HPos>()
    var titleHAlign by titleHAlignProperty

    private val contentPane = StackPane()
    private val titleLabel = Label()

    init {
        stylesheet {
            CSS.titledBorder {
                borderInsets = multi(box(1.em, 2.px, 2.px, 2.px))
                borderWidth = multi(box(0.5.px, 0.5.px, 0.5.px, 0.5.px))
                borderStyle = multi(BorderStrokeStyle.DOTTED)
                borderRadius = multi(box(5.px))
                unsafe("-fx-background-color", PropertyHolder.Raw("-fx-background"))

                CSS.titledBorderTitle {
                    translateY = -1.em
                    padding = box(0.0.px, 10.0.px)
                    unsafe("-fx-background-color", raw("inherit"))
                }

                CSS.titledBorderContent {
                    padding = box(1.em, 10.px, 8.px, 10.px)
                }
            }
        }

        contentProperty.onChange {
            if (it != null) {
                contentPane.replaceChildren(it)
            } else contentPane.children.clear()
        }

        titleHAlignProperty.onChange {
            if (it != null) titleLabel.stackpaneConstraints {
                when (it) {
                    HPos.CENTER -> alignment = Pos.TOP_CENTER
                    HPos.LEFT -> {
                        alignment = Pos.TOP_LEFT; marginLeft = 10.0
                    }
                    HPos.RIGHT -> {
                        alignment = Pos.TOP_RIGHT; marginRight = 10.0
                    }
                }
            }
        }

        this.content = content
        this.title = title
        this.titleHAlign = align

        titleLabel.textProperty().bind(titleProperty)

        this.addClass(CSS.titledBorder)
        titleLabel.addClass(CSS.titledBorderTitle)
        contentPane.addClass(CSS.titledBorderContent)

        this.children.addAll(titleLabel, contentPane)
    }

    constructor() : this("", null)

    fun contentBuilder(action: Pane.() -> Unit) {
        contentPane.apply(action)
    }

    companion object {
        object CSS {
            val titledBorder by cssclass()
            val titledBorderTitle by cssclass()
            val titledBorderContent by cssclass()
        }
    }
}

fun EventTarget.titledBorder(title: String, content: Node? = null) =
    opcr(this, TitledBorder(title, content))

fun EventTarget.titledBorder(title: String, content: Node? = null, builder: TitledBorder.() -> Unit): TitledBorder =
    opcr(this, TitledBorder(title, content), builder)

fun EventTarget.titledBorder(builder: TitledBorder.() -> Unit) =
    opcr(this, TitledBorder(), builder)
