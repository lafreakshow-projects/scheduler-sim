/*
 * File:   scheduler-sim.sim-gui.main/StatusBarView.kt
 * Date:    05.06.21, 14:01
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui.views.sim


import javafx.geometry.Pos
import javafx.scene.layout.BorderStrokeStyle
import lafreakshow.simulation.scheduler.gui.MainStylesheet
import tornadofx.*

class StatusBarView: View() {
    override val root = hbox {
        stylesheet {
            statusBar {
                unsafe("-fx-background-color", PropertyHolder.Raw("derive(-fx-background, -5%)"))
                borderStyle = multi(BorderStrokeStyle.SOLID)
                borderWidth = multi(box(1.px, 0.px, 0.px, 0.px))
                padding = box(2.px, 10.px)
                fontSize = 0.9.em
            }
        }

        addClass(statusBar, MainStylesheet.spacedBox)
        alignment = Pos.CENTER

        label("STAUS: IDLE")
        spacer()
        label("TIME:          0") {
            style {
                fontFamily = "monospaced"
            }
        }
        progressbar {
            this.maxWidth = Double.MAX_VALUE
        }
    }

    companion object {
        val statusBar by cssclass()
    }
}
