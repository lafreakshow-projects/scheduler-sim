/*
 * File:   scheduler-sim.sim-gui.main/Extensions.kt
 * Date:    31.03.21, 17:50
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.gui

import tornadofx.*
import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.layout.StackPane
import javafx.scene.text.Text

/**
 * Creates a label and bind [observableValue] to it's textProperty.
 *
 * Uses TornadoFX label builder internally, so it will also attempt to add the label to its receiver.
 */
fun EventTarget.label(observableValue: ObservableValue<String>, builder: Label.() -> Unit = {}) =
    this.label() {
        textProperty().bind(observableValue)
        builder()
    }

/**
 * Creates a label with this [ObservableValue] bound to its text property
 */
fun ObservableValue<String>.label(builder: Label.() -> Unit = {}): Label =
    Label().apply {
        textProperty().bind(this@label)
        builder()
    }

fun estimateStringPixelWidth(text: String): Double {
    val text = Text(text)
    val scene = Scene(StackPane(text))
    FX.applyStylesheetsTo(scene)
    text.applyCss()
    return text.layoutBounds.width
}

fun TableView<*>.autoSizeColumns() {

    // calling estimateStringPixelWidth every time would create a new scene each time, which can be avoided
    val measuringText = Text()
    val measuringScene = Scene(StackPane(measuringText))
    FX.applyStylesheetsTo(measuringScene)
    measuringText.applyCss()

    fun stringWidth(textToMeasure: String): Double {
        measuringText.text = textToMeasure
        return measuringText.layoutBounds.width
    }

    columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY

    this.columns.forEach { column ->
        val headerWidth = stringWidth(column.text)

        var maxDataWidth = 0.0
        (0 .. this.items.size).forEach {  i ->
            if (column.getCellData(i) != null) {
                maxDataWidth = maxOf(maxDataWidth, stringWidth(column.getCellData(i).toString()))
            }
        }

        column.minWidth = maxOf(headerWidth, maxDataWidth) + 15
    }
}

fun TableColumn<*, *>.autoSize(isMaxWidth: Boolean = true) {
    this.minWidth = estimateStringPixelWidth(this.text) + 10
    if (isMaxWidth) this.maxWidth = this.minWidth
}
