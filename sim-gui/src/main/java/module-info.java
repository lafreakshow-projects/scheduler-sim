module lafreakshow.simulation.scheduler.gui {
    requires kotlin.stdlib;

    requires tornadofx;
    requires javafx.graphics;
    requires javafx.base;
    requires javafx.controls;
    requires org.kordamp.ikonli.core;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.ikonli.material2;

    exports lafreakshow.simulation.scheduler.gui;
    opens lafreakshow.simulation.scheduler.gui to javafx.graphics, tornadofx;
    opens lafreakshow.simulation.scheduler.gui.views to tornadofx;
    opens lafreakshow.simulation.scheduler.gui.views.sim to tornadofx;
}
