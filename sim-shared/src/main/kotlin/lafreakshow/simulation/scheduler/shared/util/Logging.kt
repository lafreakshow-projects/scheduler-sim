/*
 * File:   scheduler-sim.sim-shared.main/Logging.kt
 * Date:    15.04.21, 07:29
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.shared.util

import java.util.logging.Logger
import kotlin.reflect.KClass
import kotlin.reflect.full.companionObject

// NOTE: See https://stackoverflow.com/a/34462577
// I got most of this stuff from there.
/**
 * Returns a lazy logger. Can be used as a delegate
 *
 * **Note** that this function may return a cached logger instead of a newly created object. In terms of this, this
 * function behaves like Logger.getLogger(). It does not guarantee that the returned logger is unique to the specific
 * calling instance or even the calling class, if a logger of the name has been used somewhere else before. Therefore
 * when using initializer to apply configuration, it is recommended to check if the logger is already configured
 *
 * When it is expected that many instances of a class use the same logger, it is recommended to avoid initializer
 * entirely or move the logger into a companion object.
 *
 * **Note** that the returned logger is lazy. This means the initializer will not bee executed until the Lazy
 * instance returned by this function accessed for the first time.
 *
 * @param simpleName    If true, will use the classes simpleName instead of its qualified name
 * @param identifier    If not null, will be appended to the end of the name. Example use: appending an instance ID
 *                      to a logger to create a logger unique to an instance of a class.
 * @param initializer   An optional function that will be run with the new logger as "this" Intended to add custom
 *                      configuration to the logger.
 */
fun <R : Any> R.logger(
    simpleName: Boolean = false,
    identifier: String? = null,
    initializer: (Logger.() -> Unit)? = null,
) =
    generateLoggerForClass(this, simpleName, null, identifier, initializer)

/**
 * Returns a lazy logger. Can be used as a delegate
 *
 * **Note** that this function may return a cached logger instead of a newly created object. In terms of this, this
 * function behaves like Logger.getLogger(). It does not guarantee that the returned logger is unique to the specific
 * calling instance or even the calling class, if a logger of the name has been used somewhere else before. Therefore
 * when using initializer to apply configuration, it is recommended to check if the logger is already configured
 *
 * When it is expected that many instances of a class use the same logger, it is recommended to avoid initializer
 * entirely or move the logger into a companion object.
 *
 * **Note** that the returned logger is lazy. This means the initializer will not bee executed until the Lazy
 * instance returned by this function accessed for the first time.
 *
 * @param name          A String to be used as the name for this logger.
 * @param identifier    If not null, will be appended to the end of the name. Example use: appending an instance ID
 *                      to a logger to create a logger unique to an instance of a class.
 * @param initializer   an optional function that will be run with the new logger as "this" Intended to add custom
 *                      configuration to the logger.
 */
fun <R : Any> R.logger(name: String, identifier: String? = null, initializer: (Logger.() -> Unit)?) =
    generateLoggerForClass(this, false, name, identifier, initializer)

/**
 * Generate logger for a given object.
 *
 * @param R            Type of the target
 * @param target       The object to create a logger for
 * @param simpleName   Whether to use the objects qualified class name or the simple class name
 * @param identifier   If not null, will be appended to the end of the name. Example use: appending an instance ID
 *                     to a logger to create a logger unique to an instance of a class.
 * @param name         Name of the logger. If this is not null, simpleName and target will be ignored.
 * @param initializer  A function to execute with the generated logger as "this"
 *
 * @return A Lazy encapsulating the new logger.
 */
internal fun <R : Any> generateLoggerForClass(
    target: R,
    simpleName: Boolean,
    name: String?,
    identifier: String?,
    initializer: (Logger.() -> Unit)?,
): Lazy<Logger> {
    return lazy {
        var loggerName = if (name.isNullOrBlank()) {
            if (simpleName) unwrapCompanionClass(target::class).simpleName else {
                unwrapCompanionClass(target::class).qualifiedName
            }
        } else name

        if (!identifier.isNullOrBlank()) {
            loggerName += identifier
        }

        Logger.getLogger(loggerName).also {
            if (initializer != null) {
                it.initializer()
            }
        }
    }
}

// unwrap companion class to enclosing class given a Java Class
internal fun <T : Any> unwrapCompanionClass(ofClass: Class<T>): Class<*> {
    return if (ofClass.enclosingClass != null && ofClass.enclosingClass.kotlin.companionObject?.java == ofClass) {
        ofClass.enclosingClass
    } else {
        ofClass
    }
}

// unwrap companion class to enclosing class given a Kotlin Class
internal fun <T : Any> unwrapCompanionClass(ofClass: KClass<T>): KClass<*> = unwrapCompanionClass(ofClass.java).kotlin
