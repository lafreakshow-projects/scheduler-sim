/*
 * File:   scheduler-sim.sim-shared.main/Exceptions.kt
 * Date:    19.04.21, 08:11
 *
 * Copyright 2021 Lafreakshow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lafreakshow.simulation.scheduler.shared.util

import kotlin.RuntimeException

/**
 * Base class for exceptions belonging to the Scheduler Sim. As subclass of [RuntimeException] this is an unchecked
 * exception. Use it for less serious problem that may/can be silently ignored.
 */
open class SimRuntimeException(msg: String, cause: Throwable? = null) : RuntimeException(msg, cause)

/**
 * Indicates that a method or function was called at an inappropriate time. This usually means some part of the
 * Simulation needs to be initialised but hasn't been yet or some process has already finished/begun and cannot be
 * repeated/started multiple times.
 */
open class SimIllegalStateException(msg: String, cause: Throwable? = null) : SimRuntimeException(msg, cause)

/**
 * Indicates that a function or method has received and unexpected argument that it cannot handle. Classic example are
 * numeric values outside of an allowed range.
 */
open class SimIllegalArgumentException(msg: String, cause: Throwable? = null) : SimRuntimeException(msg, cause)
