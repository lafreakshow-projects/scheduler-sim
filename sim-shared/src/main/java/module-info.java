module lafreakshow.simulation.scheduler.shared {
    requires kotlin.stdlib;
    requires kotlin.reflect;
    requires transitive java.logging;

    exports lafreakshow.simulation.scheduler.shared.util;
}
